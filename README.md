# Curso Python3

## About

    - Material Curso Python3
    
## Usage

    - referencias: pasta com os arquivos, livros e apostilas de referencia do curso
    - scripts: scripts de sala de aula do curso
    - slides: slides das aulas


## Download

    - git clone  https://gitlab.com/cedon/curso-python3.git
    - cd curso-python3