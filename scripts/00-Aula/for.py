#!/usr/bin/python3
# Nome do arquivo: for.py

for i in range(1,5):
	print(i)
else:
	print('O loop for terminou.')

# Saída:
# $ python3 for.py
# 1
# 2
# 3
# 4
# O loop for terminou.
