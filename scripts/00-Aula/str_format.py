#!/usr/bin/python3
#Filename: str_format.py

idade = 25
nome = 'José'
print('{0} tem {1} anos de idade'.format(nome, idade))
print('Por que {0} está brincando de Python?'.format(nome))

print('{0:_>11}'.format('hello'))
