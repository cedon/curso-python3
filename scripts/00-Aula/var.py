#!/usr/bin/python3
#Filename: var.py

i = 5

# Soma
i = i + 5
s = 'A soma de i + 5 é:'
print(s,i)

# Subtração
i = i - 3
s = 'A subtração de i - 3 é:'
print(s,i)

#Multiplicação
i = i * 2
s = 'A multiplicação de i x 2 é:'
print(s,i)

#Divisão
i = i / 5
s = 'A divisão de i por 5 é:'
print(s,i)
