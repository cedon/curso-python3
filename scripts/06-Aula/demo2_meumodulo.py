#!/usr/bin/python3
# Nome do arquivo: demo2_meumodulo.py
 
from meu_modulo import sayhi, __version__
 
sayhi()
print('Versão', __version__)

#Saída:
#   $ python demo2_meumodulo.py
#   Olá, este é demo2_meumodule falando.
#   Versão 0.1

