#!/usr/bin/python3
# Nome do arquivo: demo_meumodulo.py
 
import meu_modulo
 
meu_modulo.sayhi()
print('Versão', meu_modulo.__version__)

#Saída:
#   $ python demo_meumodulo.py
#   Olá, este é demo_meumodulo falando.
#   Versão 0.1

