#!/usr/bin/python3
# Arquivo: aluno.py

from aula8.heranca import MembroEscola

class Estudante(MembroEscola):
    '''Representa um aluno.'''

    def __init__(self, nome, idade, media):
        MembroEscola.__init__(self, nome, idade)
        self.media = media
        print('(Iniciado Estudante: {0})'.format(self.nome))

    def tell(self):
        MembroEscola.tell(self)
        print('Média: "{0:d}"'.format(self.media))
