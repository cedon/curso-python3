#!/usr/bin/python3
# Arquivo: heranca.py

class MembroEscola:
    '''Representa qualquer membro da escola.'''

    def __init__(self, nome, idade):
        self.nome = nome
        self.idade = idade
        print('(Iniciado MembroEscola: {0})'.format(self.nome))

    def tell(self):
        '''Imprime os detalhes desta instância.'''
        print('Nome:"{0}" Idade:"{1}"'.format(self.nome, self.idade), end=" ")
