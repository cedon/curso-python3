#!/usr/bin/python3
# Arquivo: professor.py

from aula8.heranca import MembroEscola

class Professor(MembroEscola):
    '''Representa um professor.'''

    def __init__(self, nome, idade, salario):
        MembroEscola.__init__(self, nome, idade)
        self.salario = salario
        print('(Iniciado Professor: {0})'.format(self.nome))

    def tell(self):
        MembroEscola.tell(self)
        print('Salário: "{0:d}"'.format(self.salario))
