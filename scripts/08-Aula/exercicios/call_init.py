#!/usr/bin/python3
# Arquivo: class_init.py

class Person:
    def __init__(self, arg1):
        self.name = arg1
    def sayHi(self):
        print('Hello, my name is', self.name)

p = Person('Swaroop')
p.sayHi()