#!/usr/bin/python3
# Arquivo: helloworld.py

from aula8.aluno import Estudante
from aula8.professor import Professor

t = Professor('Mrs. Shrividya', 40, 30000)
s = Estudante('Swaroop', 25, 75)

print()  # imprime uma linha em branco

membros = [t, s]
for membro in membros:
    membro.tell()  # funciona tanto para Teachers como para Students