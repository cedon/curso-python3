#!/usr/bin/python3
# Arquivo: heranca.py
 
class MembroEscola:
    '''Representa qualquer membro da escola.'''
    def __init__(self, nome, idade):
        self.nome = nome
        self.idade = idade
        print('(Iniciado MembroEscola: {0})'.format(self.nome))
 
    def tell(self):
        '''Imprime os detalhes desta instância.'''
        print('Nome:"{0}" Idade:"{1}"'.format(self.nome, self.idade), end=" ")
 
class Professor(MembroEscola):
    '''Representa um professor.'''
    def __init__(self, nome, idade, salario):
        MembroEscola.__init__(self, nome, idade)
        self.salario = salario
        print('(Iniciado Professor: {0})'.format(self.nome))
 
    def tell(self):
        MembroEscola.tell(self)
        print('Salário: "{0:d}"'.format(self.salario))
 
class Estudante(MembroEscola):
    '''Representa um aluno.'''
    def __init__(self, nome, idade, media):
        MembroEscola.__init__(self, nome, idade)
        self.media = media
        print('(Iniciado Estudante: {0})'.format(self.nome))
 
    def tell(self):
        MembroEscola.tell(self)
        print('Média: "{0:d}"'.format(self.media))
 
t = Professor('Mrs. Shrividya', 40, 30000)
s = Estudante('Swaroop', 25, 75)
 
print() # imprime uma linha em branco
 
membros = [t, s]
for membro in membros:
    membro.tell() # funciona tanto para Teachers como para Students
