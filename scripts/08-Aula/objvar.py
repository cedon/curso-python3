#!/usr/bin/python3
# Arquivo: objvar.py
 
class Robot:
    '''Representa um robô com um nome.'''
 
    # Uma variável de classe, para contar o número de robôs.
    population = 0
 
    def __init__(self, name):
        '''Inicia os dados.'''
        self.name = name
        print('(Iniciando {0})'.format(self.name))
 
        # Quando este robô é criado ele é contabilizado
        # na população geral de robôs
        Robot.population += 1
 
    def __del__(self):
        '''Estou morrendo.'''
        print('{0} sendo destruído!'.format(self.name))
 
        Robot.population -= 1
 
        if Robot.population == 0:
            print('{0} era o último.'.format(self.name))
        else:
            print('Existe(m) ainda {0:d} robô(s) trabalhando.'.format(Robot.population))
 
    def sayHi(self):
        '''Saudações do robô.
 
        Sim, eles podem fazer isso.'''
        print('Saudações, meus mestres me chamam {0}.'.format(self.name))
 
    def howMany(klass):
        '''Imprime a população atual.'''
        print('Temos um total de {0:d} robô(s).'.format(Robot.population))
    howMany = classmethod(howMany)
 
droid1 = Robot('R2-D2')
droid1.sayHi()
Robot.howMany()
 
droid2 = Robot('C-3PO')
droid2.sayHi()
Robot.howMany()
 
print("\nRobôs podem realizar algum trabalho aqui.\n")
 
print("Robôs terminaram seus trabalhos. Então vamos destruí-los.")
del droid1
del droid2
 
Robot.howMany()
