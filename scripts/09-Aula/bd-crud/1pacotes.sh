#!/bin/bash

sudo apt update -qq && apt upgrade -y -qq

sudo apt install libpq-dev python3-dev python3-pip postgresql-server-dev-11 postgresql-11 -y -qq

sudo pip3 install psycopg2 psycopg2-binary Flask-SQLAlchemy Flask Flask-modus ipython setuptools
