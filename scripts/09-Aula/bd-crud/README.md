# Curso Python3

## About

    - Material Curso Python3 Aula 9

## Usage

    - app.py: aplicação com o CRUD utilizando banco de dados POSTGRES
    - app_restful.py: exercicio para correção
    - 1pacotes.sh: script de instalação dos pacotes necessário para a aplicação funcionar
    - 2criar_db.sh: script de criação da base de dados
    - 3popular.py: script popula a base de dados com as tabelas
    - templates: pasta com os templates .html das páginas da aplicação

## Execução

    - ./1pacotes.sh
    - ./2criar_db
    - ./popular_db.py
    - ./app.py
    --- <Opcional> FLASK_APP=app.py flask run --host=0.0.0.0 
    --- <Opcional> python3 app.py

## Acesso no navegador

    - http://ip_do_host:porta_configurada/

