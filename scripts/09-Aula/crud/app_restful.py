#!/usr/bin/env python3

from flask import Flask, request, redirect, url_for, render_template
from student import Student
from flask_modus import Modus
from flask_restful import Resource, Api

app = Flask(__name__)
modus = Modus(app)
api = Api(app)

students = [Student('Carlos', 'Nogueira')]

def find_student(student_id):
    return [student for student in students if student.id == student_id][0]

class Root(Resource):
	def get(self):
		return redirect(url_for('index'))

class Index(Resource):
	def post(self):
		new_student = Student(request.form['first_name'], request.form['last_name'])
		students.append(new_student)
		return	redirect(url_for('index'))

	def get(self):
		return render_template('index.html', students=students)

class New(Resource):
	def get(self):
		return render_template('new.html')

class Show(Resource):
	def get(self, id):
		return render_template('show.html', student=found_student)
	def patch(self, id):
		found_student = find_student(id)
		found_student.first_name = request.form['first_name']
		found_student.last_name = request.form['last_name']
		return redirect(url_for('index'))
	def delete(self, id):
		found_student = find_student(id)
		students.remove(found_student)
		return redirect(url_for('index'))

class Edit(Resource):
	def get(self,id):
		found_student = find_student(id)
		return render_template('edit.html', student=found_student)

api.add_resource(Root, '/')
api.add_resource(Index, '/students')
api.add_resource(New, '/students/new')
api.add_resource(Show, '/students/<int:id>')
api.add_resource(Edit, '/students/<int:id>/edit')

if __name__ == '__main__':
	app.run(host= '0.0.0.0', debug=True, port=80)
