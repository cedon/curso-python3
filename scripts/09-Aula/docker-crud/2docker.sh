#!/bin/bash

apt install  apt-transport-https  ca-certificates  curl  gnupg2  software-properties-common -y -qq

curl -fsSL  https://get.docker.com | sh

systemctl start docker

systemctl enable docker

swapoff -a

sed -i 's/.*swap.*/#&/' /etc/fstab

apt install docker-compose -y -qq
