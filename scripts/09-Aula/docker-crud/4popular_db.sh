#!/usr/bin/env python3

from app import db # importar configurações da base de dados da app

db.create_all() # criar banco de dados da aplicação
