# Curso Python3

## About

    - Material Curso Python3 Aula 9

## Usage

    - app.py: aplicação com o CRUD utilizando banco de dados POSTGRES
    - app_restful.py: exercicio para correção
    - 1pacotes.sh: script de instalação dos pacotes necessários para a aplicação funcionar
    - 2docker.sh: script de instalaçao do docker 
    - 3criar_db.sh: script de criação da base de dados
    - 4popular.py: script popula a base de dados com as tabelas
    - Dockerfile: arquivo com as configurações da imagem docker para o app
    - docker-compose.yaml: arquivo com as configurações da infra de serviços dos containers
    - requirements.txt: arquivo com as dependencias do app
    - templates: pasta com os templates .html das páginas da aplicação

## Execução

    - ./1pacotes.sh
    - ./2docker.sh
    - ./3criar_db
    - ./4popular_db.py
    - docker-compose up -d

## Acesso no navegador

    - http://ip_do_host:porta_configurada/

