#filename restful.py

from flask import Flask, request
from flask_restful import Resource, Api

app = Flask(__name__)
api = Api(app)

class OlaMundo(Resource):
	def get(self):
		return{'sobre':'Ola Mundo!'}

	def post(self):
		some_json = request.get_json()
		return{'voce enviou': some_json}, 201

class Multi(Resource):
	def get(self, num):
		return{'resultado': num*10}

api.add_resource(OlaMundo, '/')
api.add_resource(Multi, '/multi/<int:num>')

if __name__ == '__main__':
	app.run(host='0.0.0.0', debug=True, port=80)
