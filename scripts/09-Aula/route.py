from flask import Flask, jsonify, request

app = Flask(__name__)

@app.route("/", methods=['GET', 'POST'])
def index():
	if (request.method == 'POST'):
		some_json = request.get_json()
		return jsonify({'voce enviou' : some_json}), 201
	else:
		return jsonify({'sobre':'Ola Mundo'})

@app.route("/multi/<int:num>", methods=['GET'])
def get_mult(num):
	return jsonify({"resultado": num*10})

if __name__ == '__main__':
	app.run(host= '0.0.0.0', debug=True)
